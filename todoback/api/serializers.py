from rest_framework import serializers
from api.models import Task, TaskList

class TaksListSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskList
        fields = '__all__'

class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = ('id', 'name', 'status')