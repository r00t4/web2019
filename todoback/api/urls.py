
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('tasks', views.tasks, name='tasks'),
    path('task_lists', views.task_lists, name='task_lists'),
    path('tasks/<int:task_id>', views.task_with_id, name='task_with_id'),
    path('task_lists/<int:task_list_id>', views.task_list_with_id, name='task_list_with_id'),
    path('task_lists/<int:task_list_id>/tasks', views.tasks_from_task_list, name='tasks_from_task_lists'),
]