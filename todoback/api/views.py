from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from api.models import Task, TaskList
from api.serializers import *


def index(request):
    return JsonResponse({"Hello, world. You're at the polls index.": ""})

def tasks(request):
    query_set = Task.objects.all().values()
    ans = TaskSerializer(query_set, many=True)
    return JsonResponse(ans.data, safe=False)

def task_lists(request):
    query_set = TaskList.objects.all().values()
    ans = TaksListSerializer(query_set, many=True)
    return JsonResponse(ans.data, safe=False)

def task_list_with_id(request, task_list_id):
    query_set = get_object_or_404(TaskList, pk=task_list_id)
    ans = TaskListSerializer(query_set, many=True)
    return JsonResponse(ans.data, safe=False)

def tasks_from_task_list(request, task_list_id):
    task_list = get_object_or_404(TaskList, pk=task_list_id)
    query_set = Task.objects.filter(task_list = task_list)
    ans = TaskSerializer(query_set, many=True)
    return JsonResponse(ans.data, safe=False)

def task_with_id(request, task_id):
    task = get_object_or_404(Task, pk = task_id)
    return JsonResponse(task.to_json())

# def create_task(request, task):
    

