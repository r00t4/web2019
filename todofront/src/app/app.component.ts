import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map'
import { last } from '@angular/router/src/utils/collection';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'todofront';
  task_lists = [];
  tasks = {};
  last_tasks = [];
  constructor(private http: Http){
    console.log('Hellow http CLient');
    this.get_task_lists();
  }

  getTask(){
    return this.http.get('http://127.0.0.1:8000/api/task_lists').map((res: Response) => res.json())
  }

  get_task_lists() {
    return this.getTask().subscribe(data => {
      console.log(data);
      this.task_lists = data
      this.getTasks();
    })
  }

  getTasksFromTaskList(task_list_id){
    return this.http.get('http://127.0.0.1:8000/api/task_lists/' + task_list_id + '/tasks').map((res: Response) => res.json())
  }

  getTasksOfTaskList(task_list_id){
    return this.getTasksFromTaskList(task_list_id).subscribe(data=> {
      console.log(data);
      this.tasks[task_list_id] = data;
    })
  }

  getTasks(){
    console.log(this.task_lists.length)
    this.task_lists.forEach( item=> {
      this.getTasksOfTaskList(item['id']);
    })
  }
}

